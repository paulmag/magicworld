import numpy as np
FPS = 30  #TODO This is also dcleared in Game.py.


class Person:

    container = []
    radius = 0.5  # meters

    def __init__(self, board=None, faction=None):
        self.container.append(self)

        self.name = "Person"
        self.health = 100.0
        self.fatigue = 100.0
        self.energy = 100.0
        self.mana = 100.0
        # Attributes:
        self.speed = 5.0  # meters/sec
        self.sprint_ratio = 1.8
        #TODO skills
        #TODO spells
        self.coords = np.array([
            np.random.random() * 20,
            np.random.random() * 20,
        ])
        self.sprinting = False
        self.alert_by = []
        self.set_board(board)
        self.faction = faction
        self.set_color()

    def set_color(self):
        if self.faction is not None:
            self.color = self.faction.color + np.random.normal(size=3) * 10

    def set_board(self, board):
        if board is None:
            self.board = None
        else:
            board.add_person(self)

    def collide(self, other):
        """First check if there is a collision, then push the persons
        away from eachother if there is.
        """
        distance = self.coords - other.coords      # A vector.
        distance_min = self.radius + other.radius  # A scalar.
        if np.linalg.norm(distance) < distance_min:
            offset = (np.linalg.norm(distance) - distance_min) * 0.5
            direction = distance / np.linalg.norm(distance) * offset
            self.coords  -= direction
            other.coords += direction


class AI(Person):

    faction = None
    alignementMagicFeelings = 0
    alignementAggressiveness = 0
    alignementPlayerMagicFeelings = 0
    alignementPlayerAggressiveness = 0
    alignementPlayerTrustworthyness = 0
    alignementPlayerGeneralCapabilities = 0
    alignementPlayerMagicCapabilities = 0
    alert_on = []

    def go_to(self, coord):
        self.target = coord

    def walk(self):
        direction = self.target - self.coords
        self.coords += direction / np.abs(direction) * self.speed / FPS


class Player(Person):

    def set_color(self):
        self.color = 0,255,255

    def move(self, direction):
        if (direction != np.array((0,0))).any():
            if self.sprinting and self.fatigue > 20.0 / FPS:
                ratio = self.sprint_ratio
                self.fatigue -= 20.0 / FPS
            else:
                ratio = 1
            self.coords += (
                direction / np.abs(direction) * self.speed * ratio / FPS
            )

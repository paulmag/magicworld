import pygame
import sys
from pygame.locals import *
from Person import *
from Board import Board
from Faction import Faction
from Constants import *


FPS = 30
pygame.init()
dispWidth = 800
dispHeight = 600
fpsTime = pygame.time.Clock()
setDisplay = pygame.display.set_mode((dispWidth, dispHeight))


class Game:


    def __init__(self):
        boards = []
        board_active = None

        self.board_active = Board((20, 20), background=BLACK)
        self.player = Player()
        Player.player = self.player
        self.npc_n = 25
        self.npcs = [AI() for e in xrange(self.npc_n)]
        self.factions = [Faction() for e in xrange(3)]
        for npc in self.npcs:
            npc.faction = self.factions[np.random.randint(3)]
            npc.set_color()
        self.persons = [self.player] + self.npcs
        for person in self.persons:
            self.board_active.add_person(person)

        pygame.display.set_caption('Testing')
        self.run_game()


    def run_game(self):
        direction = STILL
        while True: # main game loop

            for event in pygame.event.get(): # event handling loop
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == KEYDOWN:
                    if event.key == K_LEFT or event.key == K_a:
                        direction += LEFT
                    elif event.key == K_RIGHT or event.key == K_d:
                        direction += RIGHT
                    elif event.key == K_UP or event.key == K_w:
                        direction += UP
                    elif event.key == K_DOWN or event.key == K_s:
                        direction += DOWN
                    elif event.key == K_l:
                        self.player.sprinting = True
                elif event.type == KEYUP:
                    if event.key == K_LEFT or event.key == K_a:
                        direction -= LEFT
                    elif event.key == K_RIGHT or event.key == K_d:
                        direction -= RIGHT
                    elif event.key == K_UP or event.key == K_w:
                        direction -= UP
                    elif event.key == K_DOWN or event.key == K_s:
                        direction -= DOWN
                    elif event.key == K_l:
                        self.player.sprinting = False
            self.player.move(direction)

            for npc in self.npcs:
                npc.go_to(self.player.coords)
                npc.walk()

            # Resolve Person-Person collisions:
            for i, person in enumerate(self.persons):
                for j in range(0, i):
                    if i == j:
                        continue
                    self.persons[j].collide(person)

            # Draws background and shape of active board:
            setDisplay.fill(Board.VOID_COLOR)
            makeCell = pygame.Rect(
                dispWidth  / 2 - self.player.coords[0] * Board.METER_SIZE,
                dispHeight / 2 - self.player.coords[1] * Board.METER_SIZE,
                self.board_active.shape[0] * Board.METER_SIZE,
                self.board_active.shape[1] * Board.METER_SIZE,
            )
            pygame.draw.rect(setDisplay, self.board_active.background, makeCell)

            self.drawCell()
            pygame.display.update()
            fpsTime.tick(FPS)

    def drawCell(self):
        for person in self.board_active.persons:
            x = (person.coords[0] - self.player.coords[0]) *  \
                Board.METER_SIZE + dispWidth  / 2
            y = (person.coords[1] - self.player.coords[1]) *  \
                Board.METER_SIZE + dispHeight / 2
            makeCell = pygame.Rect(x, y, Board.METER_SIZE, Board.METER_SIZE)
            pygame.draw.rect(setDisplay, person.color, makeCell)

import numpy as np
import pygame

# Colors:
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)

# Controls:
UP    = np.array([ 0, -1])
DOWN  = np.array([ 0,  1])
LEFT  = np.array([-1,  0])
RIGHT = np.array([ 1,  0])
STILL = np.array([ 0,  0])

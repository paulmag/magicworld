class Board:

    METER_SIZE = 10  # Number of pixels per square side.
    VOID_COLOR = (150, 30, 255)

    container = []

    def __init__(self, (x_size, y_size), background=(0,0,0)):
        self.container.append(self)

        self.shape = x_size, y_size
        self.background = background
        self.persons = []

    def add_person(self, person):
        if person not in self.persons:
            self.persons.append(person)
        person.board = self

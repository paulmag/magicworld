import numpy as np


class Faction:

    def __init__(self):
        self.name = "Faction"
        self.container = []
        self.color = np.random.randint(0+25, 256-25, size=3)
    def add_person(self, person):
        self.container.append(person)

This should be a (so far) open world RPG and is initally planned to be only singleplayer.

The story is set in a world with medieval-like technology where magic exists. The plot is that some people use magic, while most are very sceptical to or hate magic. This leads to high tension and unease on both sides, and there are often conflict and fighting. Nobody feels very safe. The players ultimate goal to win the game is to bring peace to this world and this can be achieved in to primary ways:

1 - Make everybody accept the use of magic.
2 - Make everybody stop using magic.

To achieve either of this the player has to make the entire population be on the same side when it comes to the use of magic. This can be done by convincing the opposition to change their mind (by reasoning, persuation, intimidation) or eliminating/silencing them (killing, banishing, imprisonment).

Some methods obviously appear more good/evil/ruthless/just/unjust than others. The intention is that the player shall be free the use what methods he wishes, so that he can try to make the world be as good as possible, or just make peace effectively, whatever the costs. He must never choose absolutely which method(s) to use, but may use whichever one he prefers in any given situation (there are many people to be "taken care of").

The player must also never directly choose which main objective (1 or 2) to do, he must simply work towards one of them. He can at any time in the game change his mind and begin working towards the other (then, of course, he may have to undo some things).

The player must not always work directly towards one of the main causes to make progress. There is also general things to do which will aid him whichever cause he choose, and which can be done before he has made up his mind:

* Increase his own skills.
* Aquire equipment.
* Explore the world (and its people) to find out how things are, what opinions people have. This SHOULD usually be done to some degree before the player makes up his mind.
* Increase his ethos among people without neccesarily advocating a specific cause. This may make it easier to convince them to his cause later.

Mechanics

There will be fighting. How large of a part the fighting will play and how complex it should be is not yet decided. There is both regular fighting with conventional weapons/methods and the use of combat magic.

There will be magic, and probably some kind of mana bar. A key feature is that it is generally easier to do things with magic than with conventional methods, but more people will (at least initially) oppose you if you do.

There will be some kind of alignement modifier and/or reputation system. Details are not worked out yet. A key feature is that you may or may not use magic wichever main objective you choose, and you may use whichever methods you wish, but if you use magic in the PRESENCE of the magic haters they will dislike it and you will lose influence over them (which is generally bad if you are going for objective 2). The "what they don't know won't hurt them" philosophy applies here.

1 - The alignements can be general and world-wide, such that if any normal person sees you perform an action your alignements change for eveybody.
2 - Probably better is if you have a seperate set of alignements for each group of people. (magic users vs. magic haters, and probably some other groups or more specific groups).
3 - The most complex (and coolest) would be if alignements are specific for each PERSON. Then there needs to be some system for persons to (supposedly) spread the word of what they see, such that over time people that belong in a group will get the same view of you. Some groups can be more tight than others, and some groups may also communicate with each other. This means that if specific persons witness an action performed by the player, they CAN be hindered from spreading the word of his actions (by persuasive or violent methods) if he doesn't want others to find out.

A hybrid between 2 and 3 might be possible, but 3 gives the highly dynamic and free/open world and AI behavior that I strive for.

Alignement bar suggestions:
no magic - magic user - powerful magic user
magic hater - indifferent - magic lover
ruthless - neutral - gentle
chaotic - neutral - trustworthy

Suggestion if system 3: Each AI person has not only his own set of alignements for how he thinks of the player, but also his OWN set of alignements. How much he likes you or thinks of you as a threath may be determined by how your alignement is compared to his. There could also be some kind of group mentality which makes it more likely for a gentle AI to attack you if there is a ruthless one from the same group nearby (and vice verca?) (the mind boggles).